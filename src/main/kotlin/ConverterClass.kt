import kotlin.math.roundToInt

class ConverterToUsd(): CurrencyConverter2 {
    override val currencyCode: String = "USD"
    override fun convertRub2(amount: Int): Int {
        return (amount / 63.2).roundToInt()
    }
}

class ConverterToJpy(): CurrencyConverter2 {
    override val currencyCode: String = "JPY"
    override fun convertRub2(amount: Int): Int {
        return (amount / 0.54).roundToInt()
    }
}

class ConverterToGbp(): CurrencyConverter2 {
    override val currencyCode: String = "GBP"
    override fun convertRub2(amount: Int): Int {
        return (amount / 85.3).roundToInt()
    }
}

