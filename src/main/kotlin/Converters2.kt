object Converters2 {

    fun get(currencyCode: String?): CurrencyConverter2 {
        return when (currencyCode?.toUpperCase()) {
            "USD" -> ConverterToUsd()
            "JPY" -> ConverterToJpy()
            "GBP" -> ConverterToGbp()
            else -> object: CurrencyConverter2{
                override val currencyCode: String = ""
                override fun convertRub2(amount: Int): Int {
                    return 0
                }
            }
        }
    }
}