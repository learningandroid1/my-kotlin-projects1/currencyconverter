object Converters {
    private val converterToUsd = object : CurrencyConverter {
        override val currencyCode: String = "USD"
        override fun convertRub(amount: Int) = println("$amount рублей = ${amount / 69.5} $currencyCode")
    }

    private val converterToEur = object : CurrencyConverter {
        override val currencyCode: String = "EUR"
        override fun convertRub(amount: Int) = println("$amount рублей = ${amount / 74.5} $currencyCode")
    }

    private val converterToCny = object : CurrencyConverter {
        override val currencyCode: String = "CNY"
        override fun convertRub(amount: Int) = println("$amount рублей = ${amount / 10.11} $currencyCode")
    }

    private val converterToHZ = object : CurrencyConverter {
        override val currencyCode: String = ""
        override fun convertRub(amount: Int) = println("Такого конвертера нет")
    }

    fun get(currencyCode: String?): CurrencyConverter {
        return when (currencyCode?.toUpperCase()) {
            "USD" -> converterToUsd
            "EUR" -> converterToEur
            "CNY" -> converterToCny
            else -> converterToHZ
        }
    }
}

