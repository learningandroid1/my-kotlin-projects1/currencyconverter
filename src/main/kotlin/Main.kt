fun main() {

    val code = usersInput()
    val amount = usersInput2()

    println(Converters.get(code).convertRub(amount))
}

fun usersInput(): String? {
    println("Enter a currency code (3 letters)")
    return readlnOrNull()
}

fun usersInput2(): Int {
    println("Enter an amount of rubles")
    var n = readLine()?.toIntOrNull() ?: return 0
    n = if (n > 0) n
    else { println("Please, enter a number > 0")
        val m = usersInput2()
        m
    }
    return n
}