interface CurrencyConverter2 {
    val currencyCode: String
    fun convertRub2(amount: Int): Int
}