interface CurrencyConverter {
    val currencyCode: String
    fun convertRub(amount: Int)
}