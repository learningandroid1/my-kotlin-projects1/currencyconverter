fun main() {
    val code = usersInput()
    val amount = usersInput2()
    val converter = Converters2.get(code)

    println("$amount руб = ${converter.convertRub2(amount)} ${code?.toUpperCase()}")
}